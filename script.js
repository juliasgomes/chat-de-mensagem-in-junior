function send_message(){
    let input = document.querySelector(".message_input");
    let escopo = document.createElement("div");
    let section = document.querySelector(".message_section");
    escopo.innerHTML = `
        <div class="escreve">
            ${input.value}
        </div>
        <div class="button">
            <button class="editar" onclick="editar(this)">Editar</button>
            <button class="excluir" onclick="excluir(this)">Excluir</button>
        </div>
    `
    section.appendChild(escopo);
    input.value = "";
}

let send_button = document.querySelector(".send_btn");
send_button.addEventListener("click", (e)=>{
    e.preventDefault();
    send_message();
});

let edit_button = document.querySelector(".editar");
edit_button.addEventListener("click", (e)=>{
    e.preventDefault();
    editing();
});

function excluir(isso){
    isso.parentNode.parentNode.remove();
}

/* function editar(isso){
    isso.parentNode.parentNode.firstElementChild.innerText = input.value;
} */

